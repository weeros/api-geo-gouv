<?php

namespace Weeros;

use GuzzleHttp\Client;

class ApiAdresseGouvServices
{
    protected $client;

    protected $urlApi = 'https://api-adresse.data.gouv.fr';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getQuery($url) {
        $request = $this->client->get($url);
        if($request->getStatusCode() == 200)
        {
            return json_decode($request->getBody()->getContents());
        }
        else
        {
            return [null, null, null];
        }
    }
    /**
     * @param $address
     * @param $zipCode
     * @param $city
     * @return array|int[longitude,latitude]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getLatitudeLongitude($zipCode, $city, $address = "") {
        $query = $address ? $address . ' ' . $zipCode . ' '. $city : $zipCode . ' '. $city;
        return $this->getQuery($this->urlApi . '/search/?q='.$query.'&limit=1');
    }

    public function getRegions() {
        return $this->getQuery($this->urlApi . '/regions');
    }

    public function getDepartements() {
        return $this->getQuery($this->urlApi . '/departements');
    }

    public function getCommunes() {
        return $this->getQuery($this->urlApi . '/communes');
    }

    public function getCommuneByInseeCode($codeInsee) {
        return $this->getQuery($this->urlApi . '/communes/'.$codeInsee.'?fields=code,nom,centre,codesPostaux,population,surface,contour,codeDepartement');
    }

    public function getCommunesByDepartement($codeInsee) {
        return $this->getQuery($this->urlApi . '/departements/'.$codeInsee.'/communes?fields=code,nom,centre,codesPostaux,population,surface,contour,codeDepartement');
    }
}
